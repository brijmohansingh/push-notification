<?php


$privateKey = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC4fkku7WalHcPf6tj7o9cli79Wg/dQU0024tOGCck55rkxjL9I
c5u56QbchymgpZjGSpDC0btbfYv0AjImdXr2WDWM/fIFodXARknyZtUlMtBK8AQA
fI/GhTM8aaOUz3kJ7z+eWbuKUJNhZc/Ezx4bTpQhOuc7wjkq9zM/uAESAwIDAQAB
AoGAEOS3bM2eNYZZNjq57ajd/XkjZxekYL2P93OjrMVeoKeo04mwxkCXxNk6oJzb
yfL/30BaqwwrXMBNPWWqE+1z+czHWNlNEoDag4cP7YTZNi3ZJr1PJHNkvM3YHGa5
4F9d4FtyYoCH1irQnkOgRP/0o4sXB4fQaX8aSWIH7W+q/HECQQDp+g7O+LVwE3Xm
bjb28xctFfpWmA3h+QD1vK4rRRFRGuZB6vPoXPQfbasYz9VgORbJgAu1ga6UHnu5
AZEQAX57AkEAydvdtpeLwBIMMeUB1bkOyaYnioncvUNdJcyL/VgcSi85wsyrV6Ge
JDsJ3EU6+UFE3K0/gTfvZzElOclx7SGoGQJAI84VaAzdTxpbR3kERkQq/sZHOeso
7LS7Nd+603pNTRkNtXar/poC7bBNp41efMwftZQ3tn3y5k3H5ybbORTRcwJBAMBb
WKhLOc1KZXyWcm0r8D+QqlWGkmd47ia7CRve/vZiknRjfVQAmyaDbB4PiudDn097
eOy2VNjNKGAs6vfB/uECQALXB34QoulvtdrmUWeICuYh7U4h4belGgduwchHgPYu
B2degEyCCGQf5Zu04eus81sc7dGgP7ftO8Q1QKAMcKw=
-----END RSA PRIVATE KEY-----
EOD;

$publicKey = <<<EOD
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC4fkku7WalHcPf6tj7o9cli79W
g/dQU0024tOGCck55rkxjL9Ic5u56QbchymgpZjGSpDC0btbfYv0AjImdXr2WDWM
/fIFodXARknyZtUlMtBK8AQAfI/GhTM8aaOUz3kJ7z+eWbuKUJNhZc/Ezx4bTpQh
Ouc7wjkq9zM/uAESAwIDAQAB
-----END PUBLIC KEY-----
EOD;

 return array(
     'private' => array(
            'Key' => $privateKey
     ),
     'public' => array(
            'Key' => $publicKey
     ),
     'apiBaseUrl' => '27.0.0.1:8000/'
 );


