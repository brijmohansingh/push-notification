<?php
namespace App\Entities;

use Eloquent;

/**
 * User entity to be used as a model for database operations
 */
class Jwttoken extends Eloquent
{
   //
    public $timestamps = true;
}