<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Carbon;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use App\AlertMessage as PushNotificationUpdate; 

class AlertNotification extends Notification
{
    use Queueable;
   public $pushNotification;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(PushNotificationUpdate $pushNotificationUpdate)
    {
        $this->pushNotification = $pushNotificationUpdate;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->pushNotification->title,
            'body' => $this->pushNotification->content,
            'created' => Carbon::now()->toIso8601String()
        ];
    }

    /**
     * Get the web push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @param  mixed  $notification
     * @return \Illuminate\Notifications\Messages\DatabaseMessage
     */
    public function toWebPush($notifiable, $notification)
    {   
        return (new WebPushMessage)
            ->title($this->pushNotification->title)
            ->body($this->pushNotification->content)
            ->action('View app', 'view_app')
            ->data(['id' => $notification->id]);
    }
}
