<?php
namespace App\Repositories\Jwttoken;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use \Firebase\JWT\JWT;

/**
 * Repository class to communicate with data sources of different kinds
 */
class JwttokenRepository implements JwttokenInterface
{

    
    const HTTP_METHOD =  'GET';
    const HTTP_POST =  'POST';
   

    // Jwttoken model reference
    protected $jwttokenModel;
    protected $http;
    protected $jwtToken;
    protected $data;
    
    
    // JWT Token Setting
    protected $payload;
    protected $privateKey;
    protected $publicKey;
    
    

    /**
     * Assigning $jwttokenModel to injected model
     *
     * @param Model $pokemon
     * @return JwttokenRepository
     */
    public function __construct(Model $jwttoken)
    {
        $this->privateKey = config('key.private.Key');
        $this->publicKey = config('key.public.Key');
        $this->jwttokenModel = $jwttoken;
        $this->http = new Client([
            'base_uri' => config('key.apiBaseUrl'),
        ]);
       
    }

    /**
     * Returns the BearerToken
     *
     * @param mixed none
     * @return Model
     */
    
    public function getBearerToken() 
    {
    //print_r($this->generateServerAuthorisationToken());die;
        $response = $this->http->request(self::HTTP_POST,'oauth/token',[
                'form_params' => [
                    'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                    'assertion' => $this->generateServerAuthorisationToken()
                ]
            ]

        );
        
        $this->data =  $response->getBody()->getContents();
        
    return $this->data;     
    }
    
    
     public function generateServerAuthorisationToken(array $user)
    {
        
        $jwtToken = null;
        
            if ($user) {
                    //$userId = $user['id'];
                    $jwtToken = JWT::encode([
                            'sub' => $user,
                            'exp' => time() + 3600
                        ], Configure::read('Application.private.key'), 'RS256');
                }
        
        return $jwtToken;
    }
    
    public function getDecodeJWTToken($jwt)
    {
        $decoded = JWT::decode($jwt, config('key.public.Key'), array('RS256'));
        
        return $decoded;
    }

}