<?php
namespace App\Repositories\Jwttoken;

/**
 * Defining interfaces for Jwttoken repository
 */
interface JwttokenInterface
{
     /**
     * Interface to generate jwttoken 
     * 
     */
    public function generateServerAuthorisationToken($user);
    /**
     * Interface to decode JWT Token 
     * 
     */
    public function getDecodeJWTToken($jwt);
    /**
     * Interface to generate Bearer Token 
     * 
     */
    public function getBearerToken();
    
}