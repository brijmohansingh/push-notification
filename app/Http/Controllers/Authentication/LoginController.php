<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use App\User as User;

use App\Services\Jwttoken\JwttokenFacade as JwtJwttoken;

class LoginController extends Controller
{
   
    protected $userPermissionRepository;

    /**
     * LoginController constructor.
     * @param UserPermissionRepository
     */
    public function __construct()
    {  
         
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try{
            $userCheck = SecurityUser::where('username', $request->username)->first();
            if($userCheck->super_admin == config('constants.canLogIn.superAdmin') || $userCheck->is_staff == config('constants.canLogIn.isStaff')){
                $input = $request->only('username', 'password');
                $token = null;

            if (!$token = JWTAuth::attempt($input)) {
                return $this->sendErrorResponse('Invalid Email or Password');
            }
            
            return $this->sendSuccessResponse('Logged In successfully', ['token' => $token]); 
            } else {
                return $this->sendErrorResponse("You Are Not Authorized To Access This Page");
            }
                
            
        } catch (\Exception $e) {
                Log::error(
                    'Failed to fetch list from DB',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );

                return $this->sendErrorResponse("You Are Not Authorized To Access This Page");
            }
        
    }

    /**
     * getting logged in user name
     * @return mixed
     */
    public function userName()
    {
        try{
           $currentUser = JWTAuth::user()->username;
            if ($currentUser) {
                return $this->sendSuccessResponse('Logged in user', $currentUser);
            } else {
                return $this->sendSuccessResponse('User Not Found');
            } 
        } catch (\Exception $e) {
            echo $e;
                Log::error(
                    'Failed to fetch list from DB',
                    ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
                );

                return $this->sendErrorResponse("User Not Found");
            }
        
    }

    /**
     * Getting User Permission List
     * @return JsonResponse
     */
    public function permissionList()
    {
        try {
            $userData = JWTAuth::user();
            $userId = $userData->id??0;
            $permissions = $this->userPermissionRepository->userPermission($userId);

            Log::info('Fetched User Permission List From DB', [
                'method' => __METHOD__,
                'data' => ['permissions' => $permissions]
            ]);
            return $this->sendSuccessResponse('Permission List Found', $permissions); 
        } catch (\Exception $e) {
            log::error(
                'Failed to get logged in user permission list',
                ['message' => $e->getMessage(), 'trace' => $e->getTraceAsString()]
            );
            return $this->sendErrorResponse("Failed to get logged in user permission list");
        }
    }
}
