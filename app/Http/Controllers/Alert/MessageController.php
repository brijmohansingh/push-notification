<?php

namespace App\Http\Controllers\Alert;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use App\AlertMessage as Message;
use Session;



class MessageController extends Controller {

    /**
     * LoginController constructor.
     * @param UserPermissionRepository
     */
    public function __construct() {
        $this->middleware('auth');
        
    }

    public function create() {
        return view('message.create');
    }

    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required'
        ]);

        $input = $request->all();

        $message = Message::create($input);

        
          Message::where('id','<>',$message->id)
          ->update(['status' => 'INACTIVE']);

        Session::flash('flash_message', 'Notification content successfully added!');

       // return redirect()->back();
        return redirect('/message');
    }

    public function index() {
        $messages = Message::all();

        return view('message.index')->withMessages($messages);
    }

    public function show($id) {
        $message = Message::findOrFail($id);
        Message::where('id', $id)
          ->update(['status' => 'ACTIVE']);
          Message::where('id','<>', $id)
          ->update(['status' => 'INACTIVE']);

        return view('message.show')->withMessage($message);
    }
    
    public function edit($id)
{
        $message = Message::findOrFail($id);

    return view('message.edit')->withMessage($message);
    
}

public function update($id, Request $request)
{
    $message = Message::findOrFail($id);

    $this->validate($request, [
        'title' => 'required',
        'content' => 'required'
    ]);

    $input = $request->all();

    $message->fill($input)->save();

    Session::flash('flash_message', 'Successfully added!');

    return redirect()->back();
}

public function destroy($id)
{
    $message = Message::findOrFail($id);

    $message->delete();

    Session::flash('flash_message', 'Successfully deleted!');

    return redirect()->route('message.index');
}

}
