<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AlertMessage as PushNotificationUpdate; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pushNotificationUpdates = [];
        //$pushNotificationUpdate = PushNotificationUpdate::where('sent', '1')->firstOrFail();
//        $pushNotificationUpdates = PushNotificationUpdate::where('status', '')
//               ->orderBy('title', 'desc')
//               ->take(20)
//               ->get();
        return view('home', compact('pushNotificationUpdates'));
    }
}
