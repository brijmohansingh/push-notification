<?php
namespace App\Providers;

use App\Services\Jwttoken\JwttokenService;
use Illuminate\Support\ServiceProvider;

/**
 * Registering Jwttoken service
 */
class JwttokenServiceProvider extends ServiceProvider
{

    /**
     * Binding Jwttoken service
     */
    public function register()
    {
        //
    }
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('JwttokenService', function ($app) {
            return new JwttokenService(
                // Injecting user interface to be used as user repository
                $app->make('App\Repositories\Jwttoken\JwttokenInterface'));
        });
    }
}