<?php
namespace App\Providers;

use App\Entities\Jwttoken;
use App\Repositories\Jwttoken\JwttokenRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Registering the Jwttoken repository
 */
class JwttokenRepositoryServiceProvider extends ServiceProvider
{

    /**
     * Binding Jwttoken interface
     */
    public function register()
    {
       //
    }
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         $this->app->bind('App\Repositories\Jwttoken\JwttokenInterface', function ($app) {
            return new JwttokenRepository(new Jwttoken());
        });
    }
}