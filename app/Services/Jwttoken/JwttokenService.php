<?php
namespace App\Services\Jwttoken;

use App\Repositories\Jwttoken\JwttokenInterface;

/**
 * Service class containing the business logic
 */
class JwttokenService
{

    // Jwttoken repository reference
    protected $jwttokenRepo;

    /**
     * Initializing the repository reference with interface
     *
     * @param jwttokenInterface $jwttokenRepo
     */
    public function __construct(JwttokenInterface $jwttokenRepo)
    {
        $this->jwttokenRepo = $jwttokenRepo;
    }

    /**
     * Method to get Bearer Token
     *
     * @param mixed none
     * @return string
     */
    public function getBearerToken()
    {
        return $this->jwttokenRepo->getBearerToken();
    }
    
    /**
     * Method to get jwttoken details
     *
     * @param mixed $jwttoken
     * @return string
     */
    public function generateServerAuthorisationToken($user)
    {
        return $this->jwttokenRepo->generateServerAuthorisationToken($user);
    }
    
    /**
     * Method to get jwttoken details
     *
     * @param mixed $jwttoken
     * @return string
     */
    public function getDecodeJWTToken()
    {
        return $this->jwttokenRepo->getDecodeJWTToken($jwttoken);
    }
}