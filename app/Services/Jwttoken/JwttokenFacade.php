<?php
namespace App\Services\Jwttoken;

use \Illuminate\Support\Facades\Facade;

/**
 * Facade for jwttoken service
 */
class JwttokenFacade extends Facade
{

    /**
     * Returning service name
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'App\Services\Jwttoken\JwttokenService';
    }
}