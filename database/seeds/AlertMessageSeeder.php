<?php

use Illuminate\Database\Seeder;

class AlertMessageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alert_messages')->insert([
            'title' => 'Covod-19 Infection',
            'content' => 'The virus that causes COVID-19 is mainly transmitted'
            . '  <br/> through droplets generated when an infected person coughs, sneezes, <br/> or exhales. These droplets are too heavy to hang in the air, and quickly fall on floors or surfaces.',
            'sent' => 1
        ]);
        
        DB::table('alert_messages')->insert(
                [
            'title' => 'Covod-19 Infection In India',
            'content' => 'You can be infected by breathing in the virus if you are within close proximity of someone who has COVID-19, or by touching a contaminated surface and then your eyes, nose or mouth',
            'sent' => 1
        ]);
    }
}
