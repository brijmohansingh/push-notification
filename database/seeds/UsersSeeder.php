<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'NTT Admin',
            'email' => 'test@gmail.com',
            'user_type' => 1,// 1 for admin
            'password' => '$2y$10$e4LjtkDrSa3kZmy5ZwuYjOt02mewJ6ZWTwtJiFuXyP1ND4/qGH1.C'
        ]);
        
        DB::table('users')->insert([
            'name' => 'Shashank',
            'email' => 'Shashank@gmail.com',
            'user_type' => 0,// 0 for others
            'password' => '$2y$10$e4LjtkDrSa3kZmy5ZwuYjOt02mewJ6ZWTwtJiFuXyP1ND4/qGH1.C'
        ]);
    }
}
