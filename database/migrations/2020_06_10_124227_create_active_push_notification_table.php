<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivePushNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alert_messages', function (Blueprint $table) {
            //$table->integer('active')->after('content');;
            $table->enum('status', ['ACTIVE', 'INACTIVE'])->after('content')->default('ACTIVE');
             $table->dropColumn('sent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alert_messages', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}
