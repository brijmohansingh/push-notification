@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Add new message</h2></div>


                @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
                @endif

                {!! Form::open([
                'route' => 'message.store'
                ]) !!}

                <div class="form-group" >
                    @if($errors->any())
                    <div class="alert alert-danger" style="margin: : 5%;padding: 2%">
                        @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif

                </div>

                <div class="form-group">
                    {!! Form::label('title', 'Title:', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('title', null, ['class' => 'form-control']) !!}
                    </div>

                </div>
                <br/>
                <br/>
                <div class="form-group">
                    {!! Form::label('Content message', 'Content message:', ['class' => 'col-md-4 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
                    </div>

                </div>

                <div class="form-group" style="padding: 36% 0px 0px 42%">
                    {!! Form::submit('Create New Notification ', ['class' => 'btn btn-primary']) !!} 
                </div>


                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>



@stop