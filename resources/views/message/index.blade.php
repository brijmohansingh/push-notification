@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                <div class="col-md-6">
                <label  class="btn btn-info" style="margin-top:7%">Notification message list</label>
                </div>
                <div class="col-md-6">
                <h2><a href="{{ route('message.create') }}" class="btn btn-info">Add new message</a></h2>
                </div>
                
                
                
                </div>

                <div class="form-group" >
                    @foreach($messages as $message)
                    <h3>{{ $message->title }}</h3>
                    <p>{{ $message->content}}</p>
                    <p>
                        
                        <a href="{{ route('message.show', $message->id) }}" class="btn btn-info">View </a>
                        <a href="{{ route('message.edit', $message->id) }}" class="btn btn-primary">Edit </a>
                    </p>
                    <hr>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
</div>



@stop