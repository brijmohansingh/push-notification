@extends('layouts.app')

@section('content')







<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>Add new message</h2>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <a href="{{ route('message.index') }}" class="btn btn-primary">Go back to List.</a>
                    <a href="{{ route('message.edit', $message->id) }}" class="btn btn-primary">Edit </a>
                
                        </div>
                        <div class="col-md-6">
                            <notifications-demo></notifications-demo>
                        </div>
                    </div>
                     
                </div>


                <div class="form-group panel panel-default">
                    <table class="table">

                        <tbody>
                            <tr>
                                <th>Title</td>
                                <td>{{ $message->title }}</td>
                            </tr>
                            <tr>
                                <th>Content</td>
                                <td> {{ $message->content }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




@stop