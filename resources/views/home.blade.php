@extends('layouts.app')

<meta name="csrf-token" content="{{ csrf_token() }}">

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard
                <a href="{{ route('message.create') }}" class="btn btn-info col-md-offset-2">Add new message</a>
                
                
                </div>
                


                <div class="panel-body text-center">
                    
                    <notifications-demo></notifications-demo>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>



<script>
    // CSRF for all ajax call
//      $.ajaxSetup({
//    beforeSend: function(xhr, type) {
//        if (!type.crossDomain) {
//            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
//        }
//    },
//});
</script>

<script type="text/javascript">
  //  function sent(id) {
//        var person = {
//            name: $("#id-name").val(),
//            address:$("#id-address").val(),
//            phone:$("#id-phone").val()
//        }

       // $('#target').html('sending..');
       
       

//        $.ajax({
//            url: '/push-notifications/',
//            type: 'post',
//            dataType: 'json',
//            success: function (data) {
//                //$('#target').html(data.msg);
//                console.log(data);
//            }
//            //data: JSON.stringify(person)
//        });
//    }
</script>
@endsection
